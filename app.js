'use strict';

var Container = require('./components/container/container');
var container = new Container();

container.addService('system.logger', './components/logging/logger');
container.addService('system.config', './components/config');
container.addService('system.modules.loader', './components/modules/modulesLoader', ['system.logger', 'system.config']);
container.addService('system.modules.updater', './components/modules/modulesUpdater', ['system.logger', 'system.modules.loader']);
container.addService('system.watchers.factory', './components/watch/watchFactory', ['system.config', 'system.logger']);
container.addService('system.watchers.store', './components/watch/watchStore', ['system.logger', 'system.watchers.factory', 'system.config']);
container.addService('system.views.tableCreator', './components/views/tableCreator');

container.getService('system.modules.loader').loadModules(container.getService('system.config').config.modules.list);

container.getService('system.modules.updater').start();

container.getService('system.watchers.store').addWatch(function(watched){
    var moduleEntity = container.getService('system.modules.loader').getModule("os");
    var memData = moduleEntity.data.memory;
    watched(100 - memData.free / memData.total * 100);
}, {
    view: {
        title: "Memory usage",
        colour: "#e40404",
        height: "200px",
        updateInterval: 100,
        maxValue: 100
    },
    component: {
        updateInterval: 100,
        storeSize: 600,
        transformers: [
        ]
    }
});


container.getService('system.watchers.store').addWatch(function(watched, error, parameters){
    watched();
}, {
    view: {
        title: "DNS lookup time",
        height: "200px",
        updateInterval: 100,
        fill: "no"
    },
    component: {
        updateInterval: 100,
        storeSize: 60,
        transformers: [
            {transformer: "dnsLookupTransformer", parameters: {url: 'yandex.ru'}},
            {transformer: "timeTransformer"},
            {transformer: "ignoreErrorTransformer"}
        ]
    }
});

container.getService('system.watchers.store').addWatch(function(watched){
    var a = 1;
    for (var i = 0; i < 1000; i++) a++;
    watched();
}, {
    view: {
        title: "1000 loop time",
        height: "200px",
        updateInterval: 100,
        fill: "no"
    },
    component: {
        updateInterval: 100,
        storeSize: 60,
        transformers: [
            {transformer: "timeTransformer"},
            {transformer: "ignoreErrorTransformer"}
        ]
    }
});

container.getService('system.watchers.store').addWatch(function(watched, error, parameters){
    watched();
}, {
    view: {
        title: "RSS posts",
        height: "200px",
        updateInterval: 100,
        fill: "no"
    },
    component: {
        updateInterval: 1000,
        storeSize: 1,
        transformers: [
            {transformer: "httpRequestTransformer", parameters: {url: 'http://lostfilm.tv/rss.xml'}},
            {transformer: "xmlParserTransformer"},
            {transformer: "objectValueTransformer", parameters: {valuePath: 'rss.channel.0.item'}}
        ]
    }
});

var Twig = require("twig"),
    express = require('express'),
    app = express();

app.get('/watch/:watchId', function(req, res){
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify({data: container.getService('system.watchers.store').getWatchData(req.params.watchId)}));
});
 
// This section is optional and used to configure twig. 
app.set("twig options", {
    strict_variables: false
});

app.use('/public', express.static('public'));
 
app.get('/', function(req, res){
    res.render('index.twig', {
        modules : container.getService('system.modules.loader').getModules()
    });
});

app.get('/module/:moduleId', function(req, res){
    var module = container.getService('system.modules.loader').getModule(req.params.moduleId);
    res.render('module/module.twig', {
        modules : container.getService('system.modules.loader').getModules(),
        module : module,
        table : container.getService('system.views.tableCreator').createTable(module.data),
        watchers: container.getService('system.watchers.store').getList()
    });
});
 
app.listen(9999);