'use strict';

var ModuleInterface = require("./../moduleInterface");

class LoggerModule extends ModuleInterface {
    
    constructor() {
        super();
        
        this.route = "logger";
        this.updateInterval = 100;
    }
    
    about(){
        return {
            "name": "Logger",
            "description": "Logs"
        };
    }
    
    update() {
        this.data = this.logger.getLog();
    }
    
}

module.exports = LoggerModule;