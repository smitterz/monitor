'use strict';

class ModuleInterface {
    
    constructor() {
        this._dataVersion = 0;
        this._data = {};
        this.logger = null;
        this.updateInterval = 1000;
        this.route = '#';
    }
    
    about() {
        return {
            "name": "Basic module interface",
            "description": "Basic module interface. All modules must inherit from this interface."
        };
    }
    
    init(){
    }
    
    onLoaded() {
    }
    
    update() {
    }
    
    set data(data) {
        this._dataVersion++;
        this._data = data;
    }
    
    get data() {
        return this._data;
    }
    
    isDataActual(version) {
        return version === this._dataVersion;
    }
    
}

module.exports = ModuleInterface;