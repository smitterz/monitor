'use strict';

var ModuleInterface = require("./../moduleInterface");
var os = require("os");

class OSModule extends ModuleInterface {
    
    constructor() {
        super();
        
        this.route = 'os';
        this.updateInterval = 100;
    }
    
    about(){
        return {
            "name": "OS",
            "description": "OS and memory usage information"
        };
    }
    
    update() {
        this.data = {
            platform: os.platform(),
            arch: os.arch(),
            cpus: os.cpus(),
            memory: {
                free: os.freemem(),
                total: os.totalmem()
            }
        };
    }
    
}

module.exports = OSModule;