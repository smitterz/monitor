'use strict';

const EventEmitter = require('events');

class Container extends EventEmitter {
    
    constructor() {
        super();
        
        this.services = {};
        this.servicesConfig = {};
    }
    
    loadService(serviceName) {
        if (!this.servicesConfig[serviceName]) {
            this.getService('system.logger').log(`Container: Service ${serviceName} not exists`);
            return null;
        }
        var serviceObject = this.servicesConfig[serviceName].service;
        
        if (typeof serviceObject == 'string') {
            serviceObject = require('./../../' + serviceObject);
        }
        
        if (typeof serviceObject == 'function') {
            var diServices = [null];
            for (var i in this.servicesConfig[serviceName].parameters) {
                diServices.push(this.getService(this.servicesConfig[serviceName].parameters[i]));
            }
            serviceObject = new (Function.prototype.bind.apply(serviceObject, diServices));
        }
        
        this.services[serviceName] = serviceObject;
        this.getService('system.logger').log(`Container: Service ${serviceName} loaded successfully`);
        return serviceObject;
    }
    
    addService(serviceName, service, parameters) {
        this.servicesConfig[serviceName] = {
            service: service,
            parameters: parameters
        };
        this.getService('system.logger').log(`Container: Service ${serviceName} added to DI container`);
    }
    
    getService(serviceName) {
        var service = this.services[serviceName];
        return service ? service : this.loadService(serviceName);
    }
    
}

module.exports = Container;