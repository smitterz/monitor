'use strict';

class Logger {
    
    constructor() {
        this.logs = [];
    }
    
    log(message, messageType){
        if (messageType === undefined) {
            messageType = 'info';
        }
        
        var logEntity = {
            time: (new Date()).getTime(),
            level: messageType,
            message: message
        };
        
        this.logs.push(logEntity);
        
        console.log(message);
    }
    
    getLog(){
        return this.logs.slice(0);
    }
}

module.exports = Logger;