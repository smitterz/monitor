'use strict';

class TableCreator {
    
    createFromObject(data) {
        var names = [];
        var values = [];
        
        for (var property in data) {
            names.push(property);
            values.push(this.createTable(data[property]));
        }
        
        return {
            names: names,
            values: [values]
        };
    };
    
    createFromArray(data) {
        var names = [];
        var values = [];
        
        for (var id in data) {
            for (var property in data[id]) {
                if (names.indexOf(property) == -1) {
                    names.push(property);
                }
            }
        }
        
        for (var id in data) {
            var elementValues = [];
            for (var propertyId in names) {
                var value = data[id][names[propertyId]];
                elementValues.push(this.createTable(value));
            }
            values.push(elementValues);
        }
        
        return {
            names: names,
            values: values
        };
    };
    
    createTable(data) {
        if(data == null) {
            return "";
        }
        else if(typeof data == "object") {
            return (data instanceof Array) ? this.createFromArray(data) : this.createFromObject(data);
        }
        else {
            return data;
        }
    };
    
};

module.exports = TableCreator;