'use strict';

var TransformerInterface = require('./transformerInterface');
var dns = require('dns');

class DNSLookupTransformer extends TransformerInterface {
    
    about() {
        return "Makes DNS lookup request";
    }
    
    parameters() {
        return ['url'];
    }
    
    input() {
        return 'any';
    }
    
    output() {
        return 'integer';
    }
    
    apply(promise, parameters) {
        return promise.then((value)=>{
            return new Promise((resolve, reject) => {
                dns.lookup(parameters.url, function(err, address, family){
                    if (err) reject(Number.MAX_SAFE_INTEGER);
                    resolve();
                });
            });
        });
    }
    
}

module.exports = DNSLookupTransformer;