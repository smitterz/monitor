'use strict';

var TransformerInterface = require('./transformerInterface');

class IgnoreErrorTransformer extends TransformerInterface {
    
    about() {
        return "On error returned by throw value was output";
    }
    
    apply(promise, parameters) {
        return promise.catch((value)=>value);
    }
    
    input() {
        return 'any';
    }
    
    output() {
        return 'any';
    }
    
}

module.exports = IgnoreErrorTransformer;