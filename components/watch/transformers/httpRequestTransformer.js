'use strict';

var TransformerInterface = require('./transformerInterface');
var http = require('http');

class HTTPRequestTransformer extends TransformerInterface {
    
    about() {
        return "Output was result of http request";
    }
    
    parameters() {
        return ['url'];
    }
    
    input() {
        return 'any';
    }
    
    output() {
        return 'string';
    }
    
    apply(promise, parameters) {
        return promise.then((value)=>{
            return new Promise((resolve, reject) => {
                http.get(parameters.url, (res) => {
                    var str = ''
                    res.on('data', function (chunk) {
                        str += chunk;
                    });

                    res.on('end', function () {
                        resolve(str);
                    });
                    
                    res.resume();
                }).on('error', (e) => {
                    reject();
                });
            });
        });
    }
    
}

module.exports = HTTPRequestTransformer;