'use strict';

var TransformerInterface = require('./transformerInterface');

class ObjectValueTransformer extends TransformerInterface {
    
    about() {
        return "Return value from object by path";
    }
    
    parameters() {
        return ['valuePath'];
    }
    
    apply(promise, parameters) {
        return promise.then((value)=>{
            if (value == 'undefined') {
                return undefined;
            }
            for (var i=0, path=parameters.valuePath.split('.'), len=path.length; i<len; i++){
                if (value[path[i]] == 'undefined') {
                    return undefined;
                }
                value = value[path[i]];
            };
            return value;
        });
    }
    
    input() {
        return 'object';
    }
    
    output() {
        return 'any';
    }
    
}

module.exports = ObjectValueTransformer;