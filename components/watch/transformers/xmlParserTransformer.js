'use strict';

var TransformerInterface = require('./transformerInterface');
var parseString = require('xml2js').parseString;

class XMLParserTransformer extends TransformerInterface {
    
    about() {
        return "Result is parsed as XML input value";
    }
    
    apply(promise, parameters) {
        return promise.then((value)=>{
            return new Promise((resolve, reject) => {
                parseString(value, function (err, result) {
                    resolve(result);
                });
            });
        });
    }
    
    input() {
        return 'string';
    }
    
    output() {
        return 'object';
    }
    
}

module.exports = XMLParserTransformer;