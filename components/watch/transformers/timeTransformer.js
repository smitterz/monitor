'use strict';

var TransformerInterface = require('./transformerInterface');

class TimeTransformer extends TransformerInterface {
    
    about() {
        return "Output was time since watch iteration start in ms";
    }
    
    apply(promise, parameters) {
        return promise.then((value)=>{
            if (!value) {
                var diff = process.hrtime(parameters.timer);
                value = diff[0] * 1000 + diff[1] / 1000000;
            }
            return value;
        });
    }
    
    input() {
        return 'any';
    }
    
    output() {
        return 'float';
    }
    
}

module.exports = TimeTransformer;