'use strict';

class Watch {
    
    constructor(task, watchId, options, transform) {
        this.task = task;
        this.watchId = watchId;
        this.options = options;
        this.transform = transform;
        
        this.store = [];
        this.intervalObject = null;
        
        this.parameters = {};
    }
    
    start() {
        this.intervalObject = setInterval(()=>{
            this.update();
        }, this.options.component.updateInterval);
    }
    
    stop() {
        if (this.intervalObject) {
            clearInterval(this.intervalObject);
        }
        this.intervalObject = null;
    }
    
    update() {
        var context = this;
        
        context.parameters.timer = null;
        
        var promise = new Promise((resolve, reject)=>{
            context.parameters.timer = process.hrtime();
            try {
                return this.task(resolve, reject, context.parameters);
            } catch (e) {
                reject(e);
            }
        });
        
        promise = this.transform(promise, context.parameters, this.options.component.transformers);
        
        promise = promise.then((value)=>this.saveValue(value))
        .catch((value)=>this.saveValue(value));
    }
    
    saveValue(value) {
        this.store.push(value);
        if (this.store.length > this.options.component.storeSize) {
            this.store.shift();
        }
    }
    
    getData() {
        return this.store;
    }
    
    getOptions() {
        return this.options;
    };
    
    getId() {
        return this.watchId;
    }
    
}

module.exports = Watch;