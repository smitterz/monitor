'use strict';

class WatchStore {
    
    constructor(logger, watchFactory, config) {
        this.logger = logger;
        this.defaults = config.config.watchDefaults;
        this.watchFactory = watchFactory;
        
        this.store = [];
        this.idCounter = 0;
    }
    
    addWatch(valueFunction, options) {
        options = options || {};
        options.view = options.view || {};
        options.component = options.component || {};
        
        options = {
            "view": {
                "title":            options.view.title || this.defaults.view.title,
                "colour":           options.view.colour || this.defaults.view.colour,
                "background":       options.view.background || this.defaults.view.background,
                "width":            options.view.width || this.defaults.view.width,
                "height":           options.view.height || this.defaults.view.height,
                "updateInterval":   options.view.updateInterval || this.defaults.view.updateInterval,
                "maxValue":         options.view.maxValue || this.defaults.view.maxValue,
                "fill":             options.view.fill || this.defaults.view.fill,
                "gridWidth":        options.view.gridWidth || this.defaults.view.gridWidth
            },
            "component": {
                "updateInterval":   options.component.updateInterval || this.defaults.component.updateInterval,
                "storeSize":        options.component.storeSize || this.defaults.component.storeSize,
                "transformers":     options.component.transformers || this.defaults.component.transformers
            }
        };
        
        try {
            var watcher = this.watchFactory.create(valueFunction, this.idCounter++, options);
            watcher.start();
            this.store[watcher.getId()] = watcher;
        } catch (e) {
            this.logger.log(`Can\`t load watch: ${e.message}`, 'error');
        }
    }
    
    getWatchData(id) {
        return this.store[id].getData();
    }
    
    getList() {
        return this.store;
    }
    
}

module.exports = WatchStore;