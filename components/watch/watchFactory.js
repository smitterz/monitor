'use strict';

var Watch = require('./watch');
var transformerInterface = require(`./transformers/transformerInterface`);

class WatchFactory {
    
    constructor(config, logger) {
        this.transformers = new Map();
        this.logger = logger;
        
        for (let transformerName of config.config.watchTransformers) {
            try {
                var transformerClass = require(`./transformers/${transformerName}`);
                var transformer = new transformerClass({});
                
                // Check class inheritance
                if (!(transformer instanceof transformerInterface)) {
                    throw new Error(`Transformer not extend ${transformerInterface.name}.`);
                }
                
                this.transformers.set(transformerName, transformer);
                this.logger.log(`transformer ${transformerName} loaded`);
            } catch (e) {
                this.logger.log(`Can\`t load transformer '${transformerName}': ${e.message}`, 'error');
            }
            
        }
    }
    
    create(valueFunction, id, options) {
        for (let transformerInfo of options.component.transformers) {
            var transformer = this.transformers.get(transformerInfo.transformer);
            if (!transformer) {
                throw new Error(`Transformer '${transformerName}' used on watch '${options.view.title}' does not exists`);
            }
        }
        
        var transform = (promise, parameters, transformers) => {
            var result = promise;
            for (let transformerInfo of transformers) {
                var transformer = this.transformers.get(transformerInfo.transformer);
                Object.assign(parameters, transformerInfo.parameters);
                result = transformer.apply(result, parameters);
            }
            return result;
        };
        
        return new Watch(valueFunction, id, options, transform);
    }
    
}

module.exports = WatchFactory;
