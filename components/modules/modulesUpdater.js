'use strict';

class ModulesUpdater {
    
    constructor(logger, loader) {
        this.logger = logger;
        this.loader = loader;
        
        this.intervalObjects = [];
    }
    
    start() {
        var modules = this.loader.getModules();
        
        for (var id in modules) {
            var moduleEntity = modules[id];
            var intervalObject = setInterval(function(moduleEntity){
                moduleEntity.update();
            }, moduleEntity.updateInterval, moduleEntity);
            
            this.logger.log(`module ${moduleEntity.about().name}: started update loop with interval ${moduleEntity.updateInterval}`);
            
            moduleEntity.update();
            
            this.intervalObjects.push(intervalObject);
        }
    }
    
    stop() {
        for (var id in this.intervalObjects) {
            clearInterval(this.intervalObjects[id]);
        }
    };
    
}

module.exports = ModulesUpdater;