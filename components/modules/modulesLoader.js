'use strict';

class ModulesLoader {
    
    constructor(logger, config) {
        this.logger = logger;
        this.modulesDirectory = config.config.modules.dir;
        
        this.moduleRoutes = [];
        this.modules = [];
        
        // Interface stored in modules directory to simplify require path in modules
        this.moduleInterface = require(`./../../${this.modulesDirectory}/moduleInterface`);
    }
    
    load(moduleName) {
        try {
            var moduleClass = require(`./../../${this.modulesDirectory}/${moduleName}/module`);
            var module = new moduleClass(this.logger);
            module.logger = this.logger;
            
            // Check module inheritance from interface
            if (!(module instanceof this.moduleInterface)) {
                throw new Error(`Module not extend ${this.moduleInterface.name}.`);
            }
            
            this.moduleRoutes.push(moduleName);
            this.modules.push(module);
            this.logger.log(`module ${moduleName} loaded`);
            
            module.onLoaded();
        } catch (e) {
            this.logger.log(`Can\`t load module '${moduleName}': ${e.message}`, 'error');
        }
    }
    
    loadModules(modulesList) {
        for (var index = 0; index < modulesList.length; index++) {
            var element = modulesList[index];
            this.load(element);
        }
    }
    
    getModules() {
        return this.modules;
    }
    
    getModule(moduleName) {
        var id = this.moduleRoutes.indexOf(moduleName);
        return this.modules[id];
    }
    
}

module.exports = ModulesLoader;