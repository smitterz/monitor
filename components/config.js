'use strict';

var fs = require("fs");

class Config {
    
    constructor() {
        this.config = JSON.parse(
            fs.readFileSync(
                "./config/config.json",
                { "encoding": "UTF8" }
            )
        );
    }
}

module.exports = Config;