function watchChart(element, watchName, chartOptions, updateInterval) {
    
    var watchFunc = function(){
        $.ajax({
	        url: '/watch/' + watchName,
			type: "GET",
			dataType: "json",
			success: function(data) {
                var seria = [];
                for (var i = 0; i < data.data.length; ++i) {
                    seria.push([i, data.data[i]])
                }
                $.plot(element, [seria], chartOptions);
                setTimeout(watchFunc, updateInterval);
            }
		});
    };
    
    setTimeout(watchFunc, updateInterval);
}